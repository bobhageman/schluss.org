import './src/styles/init.less'
import start from './src/js'

(function ($) {
  // Start on document ready
  $(start)
})(window.jQuery)
